package com.techu.sostycar.services;

public class CocheDatosService {

    private String idCoche;
    private String descripcionModelo;
    private float kms;
    private float consumoKW;
    private float precioKW;
    private float costeRealE;
    private float consumoSimuladoG;
    private float precioGasolina;
    private float costeComparativoG;

    public CocheDatosService() {
    }

    public CocheDatosService(String idCoche, String descripcionModelo, float kms, float consumoKW,
                             float precioKW, float costeRealE, float consumoSimuladoG, float precioGasolina,
                             float costeComparativoG) {
        this.idCoche = idCoche;
        this.descripcionModelo = descripcionModelo;
        this.kms = kms;
        this.consumoKW = consumoKW;
        this.precioKW = precioKW;
        this.costeRealE = costeRealE;
        this.consumoSimuladoG = consumoSimuladoG;
        this.precioGasolina = precioGasolina;
        this.costeComparativoG = costeComparativoG;
    }

    public String getIdCoche() {
        return idCoche;
    }

    public void setIdCoche(String idCoche) {
        this.idCoche = idCoche;
    }

    public String getDescripcionModelo() {
        return descripcionModelo;
    }

    public void setDescripcionModelo(String descripcionModelo) {
        this.descripcionModelo = descripcionModelo;
    }

    public float getKms() {
        return kms;
    }

    public void setKms(float kms) {
        this.kms = kms;
    }

    public float getConsumoKW() {
        return consumoKW;
    }

    public void setConsumoKW(float consumoKW) {
        this.consumoKW = consumoKW;
    }

    public float getPrecioKW() {
        return precioKW;
    }

    public void setPrecioKW(float precioKW) {
        this.precioKW = precioKW;
    }

    public float getCosteRealE() {
        return costeRealE;
    }

    public void setCosteRealE(float costeRealE) {
        this.costeRealE = costeRealE;
    }

    public float getConsumoSimuladoG() {
        return consumoSimuladoG;
    }

    public void setConsumoSimuladoG(float consumoSimuladoG) {
        this.consumoSimuladoG = consumoSimuladoG;
    }

    public float getPrecioGasolina() {
        return precioGasolina;
    }

    public void setPrecioGasolina(float precioGasolina) {
        this.precioGasolina = precioGasolina;
    }

    public float getCosteComparativoG() {
        return costeComparativoG;
    }

    public void setCosteComparativoG(float costeComparativoG) {
        this.costeComparativoG = costeComparativoG;
    }

    @Override
    public String toString() {
        return "CocheDatosService{" +
                "idCoche='" + idCoche + '\'' +
                ", descripcionModelo='" + descripcionModelo + '\'' +
                ", kms=" + kms +
                ", consumoKW=" + consumoKW +
                ", precioKW=" + precioKW +
                ", costeRealE=" + costeRealE +
                ", consumoSimuladoG=" + consumoSimuladoG +
                ", precioGasolina=" + precioGasolina +
                ", costeComparativoG=" + costeComparativoG +
                '}';
    }
}
