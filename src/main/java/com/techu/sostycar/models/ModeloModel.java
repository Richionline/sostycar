package com.techu.sostycar.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "modelos")
public class ModeloModel {
    @Id
    private String idModelo;
    private String descripcion;
    private int consumoElectrico;
    private int consumoGasolina;

    public ModeloModel() {

    }

    public ModeloModel(String idModelo, String descripcion, int consumoElectrico,int consumoGasolina) {
        this.idModelo = idModelo;
        this.descripcion = descripcion;
        this.consumoElectrico = consumoElectrico;
        this.consumoGasolina = consumoGasolina;
    }

    public String getIdModelo() {
        return this.idModelo;
    }

    public void setIdModelo(String idModelo) {
        this.idModelo = idModelo;
    }
    public String getDescripcion() {
        return this.descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion= descripcion;
    }
    public int getConsumoElectrico() {
        return this.consumoElectrico;
    }
    public void setConsumoElectrico(int consumoElectrico) {
        this.consumoElectrico= consumoElectrico;
    }
    public int getConsumoGasolina() {
        return this.consumoGasolina;
    }
    public void setConsumoGasolina(int consumoGasolina) {
        this.consumoGasolina= consumoGasolina;
    }


}

