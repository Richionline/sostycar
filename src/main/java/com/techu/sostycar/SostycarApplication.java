package com.techu.sostycar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;

@SpringBootApplication
//@Import(SwaggerConfig.class)
public class SostycarApplication {

	public static void main(String[] args) {
		SpringApplication.run(SostycarApplication.class, args);
	}

}
