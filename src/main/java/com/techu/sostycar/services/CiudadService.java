package com.techu.sostycar.services;

import com.techu.sostycar.models.CiudadModel;
import com.techu.sostycar.repositories.CiudadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Clase de negocio para operar con ciudades
 */
@Service
public class CiudadService {

    /* Interfaz para trabajar contra el repositorio de ciudades */
    @Autowired
    CiudadRepository ciudadRepository;

    /**
     * Añade una nueva ciudad al repositorio
     * @param ciudadModel Objeto que encapsula los datos de la ciudad a insertar
     * @return Objeto que encapsula los datos de la ciudad insertada
     */
    public CiudadModel addCiudad(CiudadModel ciudadModel){
        System.out.println("addCiudad en ciudadService");
        return this.ciudadRepository.save(ciudadModel);
    }

    /**
     * Recupera todas las ciudades
     * @return La lista de objetos CiudadService que existen en el sistema
     */
    public List<CiudadModel> findAll(){
        System.out.println("findAll en CiudadService");

        //Con MongoDB instalado:
        return this.ciudadRepository.findAll();
    }

    /**
     * Recupera los datos de una ciudad dado su identificador
     * @param id Identificador de la ciudad a recuperar
     * @return Un objeto Optional que almacenará un objeto CiudadModel si la ciudad ha podido ser recuperada. En caso contrario el objeto almacenado será nulo.
     */
    public Optional<CiudadModel> getCiudadById(String id){
        System.out.println("getCiudadById en CiudadService");
        return this.ciudadRepository.findById(id);
    }

}
