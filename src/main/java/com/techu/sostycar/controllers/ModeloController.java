package com.techu.sostycar.controllers;

import com.techu.sostycar.models.ModeloModel;
import com.techu.sostycar.services.ModeloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/sostycar")
public class ModeloController {
    @Autowired
    ModeloService modeloService;
    @GetMapping("/modelos")
    public ResponseEntity<List<ModeloModel>> getModelos(){
        System.out.println("getModelos");
        return new ResponseEntity<>(this.modeloService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/modelos/{idModelo}")
    public ResponseEntity<Object> getModeloByIdModelo(@PathVariable String idModelo){
        System.out.println("getModeloByIdModelo en ModeloController");
        System.out.println("La id del modelo a buscar es " + idModelo);
        Optional<ModeloModel> result = this.modeloService.findByid(idModelo);
        return new ResponseEntity<>(result.isPresent() ? result.get() :
                "Modelo no encontrado", result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND );

    }
    @PostMapping("/modelos")
    public ResponseEntity<ModeloModel> addModelo(@RequestBody ModeloModel modelo){
        System.out.println("addModelo");
        System.out.println("La id del modelo" + modelo.getIdModelo());
        System.out.println("La descripcion del modelo" + modelo.getDescripcion());
        System.out.println("consumo eléctrico es " + modelo.getConsumoElectrico());
        System.out.println("consumo gasolina es " + modelo.getConsumoGasolina());
        return new ResponseEntity<>(this.modeloService.addModelo(modelo),
                HttpStatus.CREATED);
    }
}
