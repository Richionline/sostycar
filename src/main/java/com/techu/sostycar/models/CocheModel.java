package com.techu.sostycar.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "coches")
public class CocheModel {

    @Id
    private String idCoche;
    private String idModelo;
    private String matricula;

    private float consumoAcumuladoE;
    private float costeAcumuladoE;
    private float consumoSimuladoG;
    private float costeSimuladoG;

    private float kmRecorridos;

    public CocheModel() {
    }

    public CocheModel(String idCoche, String idModelo, String matricula, float consumoAcumuladoE,
                      float costeAcumuladoE, float consumoSimuladoG, float costeSimuladoG, float kmRecorridos) {
        this.idCoche = idCoche;
        this.idModelo = idModelo;
        this.matricula = matricula;
        this.consumoAcumuladoE = consumoAcumuladoE;
        this.costeAcumuladoE = costeAcumuladoE;
        this.consumoSimuladoG = consumoSimuladoG;
        this.costeSimuladoG = costeSimuladoG;
        this.kmRecorridos = kmRecorridos;
    }

    public String getIdCoche() {
        return idCoche;
    }

    public void setIdCoche(String idCoche) {
        this.idCoche = idCoche;
    }

    public String getIdModelo() {
        return idModelo;
    }

    public void setIdModelo(String idModelo) {
        this.idModelo = idModelo;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public float getConsumoAcumuladoE() {
        return consumoAcumuladoE;
    }

    public void setConsumoAcumuladoE(float consumoAcumuladoE) {
        this.consumoAcumuladoE = consumoAcumuladoE;
    }

    public float getCosteAcumuladoE() {
        return costeAcumuladoE;
    }

    public void setCosteAcumuladoE(float costeAcumuladoE) {
        this.costeAcumuladoE = costeAcumuladoE;
    }

    public float getConsumoSimuladoG() {
        return consumoSimuladoG;
    }

    public void setConsumoSimuladoG(float consumoSimuladoG) {
        this.consumoSimuladoG = consumoSimuladoG;
    }

    public float getCosteSimuladoG() {
        return costeSimuladoG;
    }

    public void setCosteSimuladoG(float costeSimuladoG) {
        this.costeSimuladoG = costeSimuladoG;
    }

    public float getKmRecorridos() {
        return kmRecorridos;
    }

    public void setKmRecorridos(float kmRecorridos) {
        this.kmRecorridos = kmRecorridos;
    }

    @Override
    public String toString() {
        return "CocheModel{" +
                "idCoche='" + idCoche + '\'' +
                ", idModelo='" + idModelo + '\'' +
                ", matricula='" + matricula + '\'' +
                ", consumoAcumuladoE=" + consumoAcumuladoE +
                ", costeAcumuladoE=" + costeAcumuladoE +
                ", consumoSimuladoG=" + consumoSimuladoG +
                ", costeSimuladoG=" + costeSimuladoG +
                ", kmRecorridos=" + kmRecorridos +
                '}';
    }
}
