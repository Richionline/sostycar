package com.techu.sostycar.repositories;

import com.techu.sostycar.models.ModeloModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ModeloRepository extends MongoRepository<ModeloModel,String> {
}

