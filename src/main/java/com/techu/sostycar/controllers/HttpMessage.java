package com.techu.sostycar.controllers;

import org.springframework.http.HttpStatus;

/**
 * Clase para encapsular los mensajes de salida hacia el consumidor HTTP
 */

public class HttpMessage {

    /** Timestamp en el que se procesa la petición */
    String timestamp;

    /** Código de retorno HTTP */
    HttpStatus status;

    /** Mensaje informativo */
    String message;

    /** URI invocada */
    String path;

    /** Objeto que contiene los datos que queremos devolver al consumidor */
    Object data;

    public HttpMessage(String timestamp, HttpStatus status,  String message, String path, Object data) {
        this.timestamp = timestamp;
        this.status = status;
        this.message = message;
        this.path = path;
        this.data = data;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public HttpStatus getStatus() {
        return status;
    }


    public String getMessage() {
        return message;
    }

    public String getPath() {
        return path;
    }

    public Object getData() {
        return data;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setRecurso(Object data) {
        this.data = data;
    }
}

