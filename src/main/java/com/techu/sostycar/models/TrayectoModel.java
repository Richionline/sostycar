package com.techu.sostycar.models;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "trayectos")
public class TrayectoModel {

    @Id
    private String idTrayecto;
    private String idCoche;
    private String idCiudadOrigen;
    private String idCiudadDestino;
    private float kms;
    private float consumoKW;
    private float precioKW;
    private float costeRealE;
    private float consumoSimuladoG;
    private float precioGasolina;
    private float costeComparativoG;

    public TrayectoModel() {
    }

    public TrayectoModel(String idTrayecto, String idCoche, String idCiudadOrigen, String idCiudadDestino,
                         float kms, float consumoKW, float precioKW, float costeRealE,
                         float consumoSimuladoG, float precioGasolina, float costeComparativoG) {
        this.idTrayecto = idTrayecto;
        this.idCoche = idCoche;
        this.idCiudadOrigen = idCiudadOrigen;
        this.idCiudadDestino = idCiudadDestino;
        this.kms = kms;
        this.consumoKW = consumoKW;
        this.precioKW = precioKW;
        this.costeRealE = costeRealE;
        this.consumoSimuladoG = consumoSimuladoG;
        this.precioGasolina = precioGasolina;
        this.costeComparativoG = costeComparativoG;
    }

    public String getIdTrayecto() {
        return idTrayecto;
    }

    public void setIdTrayecto(String idTrayecto) {
        this.idTrayecto = idTrayecto;
    }

    public String getIdCoche() {
        return idCoche;
    }

    public void setIdCoche(String idCoche) {
        this.idCoche = idCoche;
    }

    public String getIdCiudadOrigen() {
        return idCiudadOrigen;
    }

    public void setIdCiudadOrigen(String idCiudadOrigen) {
        this.idCiudadOrigen = idCiudadOrigen;
    }

    public String getIdCiudadDestino() {
        return idCiudadDestino;
    }

    public void setIdCiudadDestino(String idCiudadDestino) {
        this.idCiudadDestino = idCiudadDestino;
    }

    public float getKms() {
        return kms;
    }

    public void setKms(float kms) {
        this.kms = kms;
    }

    public float getConsumoKW() {
        return consumoKW;
    }

    public void setConsumoKW(float consumoKW) {
        this.consumoKW = consumoKW;
    }

    public float getPrecioKW() {
        return precioKW;
    }

    public void setPrecioKW(float precioKW) {
        this.precioKW = precioKW;
    }

    public float getCosteRealE() {
        return costeRealE;
    }

    public void setCosteRealE(float costeRealE) {
        this.costeRealE = costeRealE;
    }

    public float getConsumoSimuladoG() {
        return consumoSimuladoG;
    }

    public void setConsumoSimuladoG(float consumoSimuladoG) {
        this.consumoSimuladoG = consumoSimuladoG;
    }

    public float getPrecioGasolina() {
        return precioGasolina;
    }

    public void setPrecioGasolina(float precioGasolina) {
        this.precioGasolina = precioGasolina;
    }

    public float getCosteComparativoG() {
        return costeComparativoG;
    }

    public void setCosteComparativoG(float costeComparativoG) {
        this.costeComparativoG = costeComparativoG;
    }

    @Override
    public String toString() {
        return "TrayectoModel{" +
                "idCoche='" + idCoche + '\'' +
                ", idCiudadOrigen='" + idCiudadOrigen + '\'' +
                ", idCiudadDestino='" + idCiudadDestino + '\'' +
                ", kms=" + kms +
                ", consumoKW=" + consumoKW +
                ", precioKW=" + precioKW +
                ", costeRealE=" + costeRealE +
                ", consumoSimuladoG=" + consumoSimuladoG +
                ", precioGasolina=" + precioGasolina +
                ", costeComparativoG=" + costeComparativoG +
                '}';
    }
}
