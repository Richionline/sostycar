package com.techu.sostycar.services;

import com.techu.sostycar.models.CiudadModel;

import java.util.List;

public class VisitasPorCiudadMetric implements Comparable<VisitasPorCiudadMetric>{

    private String idCiudadDestino;

    private String nombreCiudadDestino;

    private Integer numVisitas;

    private List<CiudadModel> ciudadesOrigen;

    public VisitasPorCiudadMetric(String idCiudadDestino, String nombreCiudadDestino, Integer numVisitas, List ciudadesOrigen) {
        this.idCiudadDestino = idCiudadDestino;
        this.nombreCiudadDestino = nombreCiudadDestino;
        this.numVisitas = numVisitas;
        this.ciudadesOrigen = ciudadesOrigen;
    }

    public String getIdCiudadDestino() {
        return idCiudadDestino;
    }

    public void setIdCiudadDestino(String idCiudadDestino) {
        this.idCiudadDestino = idCiudadDestino;
    }

    public String getNombreCiudadDestino() {
        return nombreCiudadDestino;
    }

    public int compareTo(VisitasPorCiudadMetric visitasPorCiudadMetric){
        int resultado;

        if (this.numVisitas.intValue()<visitasPorCiudadMetric.getNumVisitas().intValue())
            resultado = 1;
        else if (this.numVisitas.intValue() == visitasPorCiudadMetric.getNumVisitas().intValue())
            resultado = 0;
        else
            resultado = -1;

        return resultado;
    }





    public void setNombreCiudadDestino(String nombreCiudadDestino) {
        this.nombreCiudadDestino = nombreCiudadDestino;
    }

    public Integer getNumVisitas() {
        return numVisitas;
    }

    public void setNumVisitas(Integer numVisitas) {
        this.numVisitas = numVisitas;
    }

    public List getCiudadesOrigen() {
        return ciudadesOrigen;
    }

    public void setCiudadesOrigen(List ciudadesOrigen) {
        this.ciudadesOrigen = ciudadesOrigen;
    }
}
