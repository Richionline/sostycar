package com.techu.sostycar.services;

import com.techu.sostycar.models.CocheModel;
import com.techu.sostycar.repositories.CocheRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CocheService {

    @Autowired
    CocheRepository cocheRepository;

    public List<CocheModel> findAll() {
        System.out.println("findAll in CocheService");

        return this.cocheRepository.findAll();
    }


    public Optional<CocheModel> findById(String id) {
        System.out.println("findById in CocheService");

        return this.cocheRepository.findById(id);
    }


    public CocheModel addCoche(CocheModel coche) {
        System.out.println("addCoche in CocheService");

        return this.cocheRepository.save(coche);
    }


    public CocheModel update(CocheModel coche) {
        System.out.println("update in CocheService");

        return this.cocheRepository.save(coche);
    }


    public boolean deleteCoche(String id) {
        System.out.println("deleteCoche in CocheService");
        boolean result = false;

        if (this.findById(id).isPresent()) {
            System.out.println("Coche a borrar encontrado!!, borrando...");
            this.cocheRepository.deleteById(id);
            result = true;
        }

        return result;
    }


    /**
     * Recupera todos los coches de la flota con la posiblidad de ordenar por un campo dado y en orden ascendente o descendente
     * @param campoOrdenacion Campo por el cual se quiere ordenar. Puede ser nulo si no se quiere ordenar por ningún campo.
     * @param sentidoOrdenacion ASC: Orden ascendente | DESC: Orden descendente. Puede ser nulo si no se quiere ordenar en ningún sentido explícito.
     * @return Una lista de objetos CocheModel que encapsulan los resultados de la búsqueda (ordenados en base al criterio establecido si este existe)
     */
    public List<CocheModel> findAllOrdered(String campoOrdenacion, String sentidoOrdenacion){
        System.out.println("findAllOrdered en CocheService");

        List<CocheModel> result;

        if (campoOrdenacion==null){ //NO hay criterios de ordenación
            result = this.cocheRepository.findAll();
        }
        else{ // Hay criterios de ordenación
            Sort.Order orderCriteria;

            if (sentidoOrdenacion != null && (sentidoOrdenacion.equalsIgnoreCase("ASC")))
                orderCriteria = new Sort.Order(Sort.Direction.ASC, campoOrdenacion);
            else if ((sentidoOrdenacion != null && (sentidoOrdenacion.equalsIgnoreCase("DESC"))))
                orderCriteria = new Sort.Order(Sort.Direction.DESC, campoOrdenacion);
            else
                orderCriteria = new Sort.Order(null, campoOrdenacion);

            result = this.cocheRepository.findAll(Sort.by(orderCriteria));
        }

        return result;
    }



}
