package com.techu.sostycar.models;


import com.techu.sostycar.models.CocheModel;

/**
 * Clase que encapsula el ahorro de cada coche de mi flota
 */
public class AhorroCocheBean extends CocheModel {

    private Float ahorroEuros;

    public AhorroCocheBean(String idCoche, String idModelo, String matricula, float consumoAcumuladoE, float costeAcumuladoE, float consumoSimuladoG, float costeSimuladoG, float kmRecorridos, Float ahorroEuros) {
        super(idCoche, idModelo, matricula, consumoAcumuladoE, costeAcumuladoE, consumoSimuladoG, costeSimuladoG, kmRecorridos);
        this.ahorroEuros = ahorroEuros;
    }

    public AhorroCocheBean(CocheModel cocheModel, Float ahorroEuros) {

        super.setIdCoche(cocheModel.getIdCoche());

        super.setIdModelo(cocheModel.getIdModelo());
        super.setMatricula(cocheModel.getMatricula());
        super.setConsumoAcumuladoE(cocheModel.getConsumoAcumuladoE());
        super.setCosteAcumuladoE(cocheModel.getCosteAcumuladoE());
        super.setConsumoSimuladoG(getConsumoSimuladoG());
        super.setCosteSimuladoG(cocheModel.getCosteSimuladoG());
        super.setKmRecorridos(cocheModel.getKmRecorridos());

        this.ahorroEuros = ahorroEuros;
    }

    public Float getAhorroEuros() {
        return ahorroEuros;
    }

    public void setAhorroEuros(Float ahorroEuros) {
        this.ahorroEuros = ahorroEuros;
    }
}
