package com.techu.sostycar.models;

public class DistanciaModel {

    private String idCiudadDestino;

    private String nombreCiudadDestino;

    private Integer distanciaKMS;

    public DistanciaModel() {
    }

    public DistanciaModel(String idCiudadDestino, String nombreCiudadDestino, Integer distanciaKMS) {
        this.idCiudadDestino = idCiudadDestino;
        this.nombreCiudadDestino = nombreCiudadDestino;
        this.distanciaKMS = distanciaKMS;
    }

    public String getIdCiudadDestino() {
        return idCiudadDestino;
    }

    public void setIdCiudadDestino(String idCiudadDestino) {
        this.idCiudadDestino = idCiudadDestino;
    }

    public Integer getDistanciaKMS() {
        return distanciaKMS;
    }

    public void setDistanciaKMS(Integer distanciaKMS) {
        this.distanciaKMS = distanciaKMS;
    }

    public String getNombreCiudadDestino() {
        return nombreCiudadDestino;
    }

    public void setNombreCiudadDestino(String nombreCiudadDestino) {
        this.nombreCiudadDestino = nombreCiudadDestino;
    }
}
