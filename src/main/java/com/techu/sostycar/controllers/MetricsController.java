package com.techu.sostycar.controllers;

import com.techu.sostycar.models.AhorroCocheBean;
import com.techu.sostycar.services.MetricsService;
import com.techu.sostycar.services.VisitasPorCiudadMetric;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * Clase controladora para la generación de métricas de valor añadido
 */
@RestController
@RequestMapping("/sostycar/")
public class MetricsController {

    /* Servicio de negocio para operar con las métricas */
    @Autowired
    MetricsService metricsService;


    /**
     * Obtiene la lista de coches de mi flota ordenados de menor a mayor consumo y añadiendo el dato del ahorro realizado con respecto
     * al uso de la versión gasolina del mismo vehículo.
     *
     * @return Código HTTP 200 - Una lista de objetos JSON que contienen los datos indicados.
     */
    @GetMapping("metrics/ahorro")
    public ResponseEntity<HttpMessage> getCochesConAhorro(){

        System.out.println("getCochesConAhorro en MetricsController...");
        ResponseEntity<HttpMessage> responseEntity;
        HttpMessage httpMessage;

        List<AhorroCocheBean> listaCochesConAhorro = this.metricsService.getListaCochesMenosConsumoConAhorro();

        httpMessage = new HttpMessage(new Date().toString(), HttpStatus.OK, "Consulta realizada con éxito", "/sostycar/metrics/ahorro", listaCochesConAhorro);

        return new ResponseEntity(httpMessage, httpMessage.getStatus());
    }

    /**
     * Obtiene la lista de ciudades ordenadas por el número de visitas que han tenido y cuales han sido las ciudades origen
     *
     * @return Código HTTP 200 - Una lista de objetos JSON que contienen los datos indicados.
     */
    @GetMapping("metrics/visitas")
    public ResponseEntity<HttpMessage> getCiudadesMasVisitadas(){

        System.out.println("getVisitasPorCiudad en MetricsController...");
        ResponseEntity<HttpMessage> responseEntity;
        HttpMessage httpMessage;

        List<VisitasPorCiudadMetric> listaMetricas = this.metricsService.getCiudadesMasVisitadas();

        httpMessage = new HttpMessage(new Date().toString(), HttpStatus.OK, "Consulta realizada con éxito", "/sostycar/metrics/visitas", listaMetricas);

        return new ResponseEntity(httpMessage, httpMessage.getStatus());
    }


}
