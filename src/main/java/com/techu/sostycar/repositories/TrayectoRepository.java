package com.techu.sostycar.repositories;

import com.techu.sostycar.models.TrayectoModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TrayectoRepository extends MongoRepository<TrayectoModel, String> {

    @Query(value = "{ 'idCoche' : ?0 }")
    List<TrayectoModel> findByIdCoche(String idCoche);

    @Query(value = "{ 'idCoche' : ?0, 'kms': ?1 }")
    List<TrayectoModel> findByIdCocheKms(String idCoche, float kms);

    // @Query("SELECT * FROM trayectos ORDER BY kms DESC")
    // List<TrayectoModel> findByKms();

}
