package com.techu.sostycar.services;

import com.techu.sostycar.models.ModeloModel;
import com.techu.sostycar.repositories.ModeloRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ModeloService {

   @Autowired
    ModeloRepository modeloRepository;
    public List<ModeloModel> findAll(){
        System.out.println("findAll en ModeloService");
        return this.modeloRepository.findAll();
    }
    public Optional<ModeloModel> findByid(String idModelo){
        System.out.println("findByidModelo en ModeloService");
        System.out.println("el modelo a buscar es" + idModelo);
        return this.modeloRepository.findById(idModelo);
    }

    public ModeloModel addModelo(ModeloModel modelo){
        System.out.println("addModelo en ModeloService");
        return this.modeloRepository.save(modelo);
    }

}

