package com.techu.sostycar.controllers;


import com.techu.sostycar.models.TrayectoModel;
import com.techu.sostycar.services.CocheDatosService;
import com.techu.sostycar.services.TrayectoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.cert.PKIXRevocationChecker;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/sostycar")
public class TrayectoController {

    @Autowired
    TrayectoService trayectoService;

    // Obtiene todos los trayectos
    @GetMapping("/trayectos")
    public ResponseEntity<HttpMessage> getTrayectos() {
        System.out.println("getTrayectos in TrayectoController");

        HttpMessage httpMessage;

        List<TrayectoModel> listaTrayectos = this.trayectoService.findAll();

        httpMessage = new HttpMessage(String.valueOf(System.currentTimeMillis()), HttpStatus.OK,
                "Consulta realizada con éxito", "/sostycar/trayectos", listaTrayectos);


        return new ResponseEntity(httpMessage, httpMessage.getStatus());
    }


    // Grabar una trayectoria
    @PostMapping("/trayectos")
    public ResponseEntity<HttpMessage> addTrayectoria(@RequestBody TrayectoModel trayecto) {
        System.out.println("addTrayectoria in TrayectoController");

        HttpMessage httpMessage;

        trayectoService.addTrayecto(trayecto);

        httpMessage = new HttpMessage(String.valueOf(System.currentTimeMillis()), HttpStatus.CREATED,
                "Trayectoria dada de alta en el sistema", "/sostycar/trayectos", trayecto);

        return new ResponseEntity(httpMessage, httpMessage.getStatus());
    }


    // Obtiene trayectos de un determinado coche
    @GetMapping("/trayectos/{idCoche}")
    public ResponseEntity<HttpMessage> getTrayectosIdCoche(@PathVariable String idCoche) {
        System.out.println("getTrayectosIdCoche in TrayectoController");
        System.out.println("Trayectos del coche con id " + idCoche);

        HttpMessage httpMessage;

        List<TrayectoModel> listaTrayectos = trayectoService.findByIdCoche(idCoche);

        httpMessage = new HttpMessage(String.valueOf(System.currentTimeMillis()), HttpStatus.OK,
                "Consulta realizada con éxito", "/sostycar/trayectos", listaTrayectos);


        return new ResponseEntity(httpMessage, httpMessage.getStatus());
    }


    // Obtiene los datos de trayectos de un coche agrupados
    @GetMapping("/trayectos/group/{idCoche}")
    public ResponseEntity<HttpMessage> getTrayectosIdCocheGroup(@PathVariable String idCoche) {
        System.out.println("getTrayectosIdCocheGroup in TrayectoController");
        System.out.println("Trayectos agrupados del coche con id " + idCoche);

        CocheDatosService cocheDatosService = trayectoService.findByIdCocheGroup(idCoche);

        HttpMessage httpMessage = new HttpMessage(String.valueOf(System.currentTimeMillis()), HttpStatus.OK,
                "Consulta realizada con éxito", "/sostycar/trayectos", cocheDatosService);


        return new ResponseEntity(httpMessage, httpMessage.getStatus());
    }


    // Obtiene los datos de todos los trayectos agrupados
    @GetMapping("/trayectos/group/total")
    public ResponseEntity<HttpMessage> getTrayectosGroup() {
        System.out.println("getTrayectosGroup in TrayectoController");
        System.out.println("Obteniendo todos los trayectos agrupados");

        CocheDatosService cocheDatosService = trayectoService.findByGroup();

        HttpMessage httpMessage = new HttpMessage(String.valueOf(System.currentTimeMillis()), HttpStatus.OK,
                "Consulta realizada con éxito", "/sostycar/trayectos", cocheDatosService);


        return new ResponseEntity(httpMessage, httpMessage.getStatus());
    }
}
