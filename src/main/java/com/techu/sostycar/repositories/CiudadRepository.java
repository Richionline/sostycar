package com.techu.sostycar.repositories;

import com.techu.sostycar.models.CiudadModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Interfaz para la operación contra el repositorio de datos de usuarios
 */
@Repository
public interface CiudadRepository extends MongoRepository<CiudadModel, String> {
}

