package com.techu.sostycar.repositories;

import com.techu.sostycar.models.CocheModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CocheRepository extends MongoRepository<CocheModel, String> {
}
