package com.techu.sostycar.controllers;


import com.techu.sostycar.models.CiudadModel;
import com.techu.sostycar.models.CocheModel;
import com.techu.sostycar.services.CocheService;
import org.apache.catalina.LifecycleState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/sostycar")
public class CocheController {

    @Autowired
    CocheService cocheService;

/*
    @GetMapping("/coches")
    public ResponseEntity<List<CocheModel>> getCoches() {
        System.out.println("getCoches in CocheController");

        return new ResponseEntity<>(this.cocheService.findAll(), HttpStatus.OK);
    }
*/


    /**
     * Obtiene todos los coches posibilitando la ordenación por un campo dado
     * @param orderBy Campo por el cual se quiere ordenar. Sigue estándar ODATA. OPCIONAL
     * @return Código HTTP 200 - Una lista de objetos JSON que contienen los datos de cada coche.
     */
    @GetMapping("/coches")
    public ResponseEntity<HttpMessage> getCoches(@RequestParam(value = "$orderBy", required = false) String orderBy) {
        System.out.println("getCoches in CocheController");
        System.out.println("El campo por el que hay que ordenar es: "+orderBy);

        ResponseEntity<HttpMessage> responseEntity;
        HttpMessage httpMessage;

        String campoOrdenacion = null;
        String sentidoOrdenacion = null;

        // Extraemos los criterios de ordenación en el caso de existan
        if (orderBy != null) {
            String[] campos = orderBy.split(" ");
            System.out.println("El número de elementos que vienen en orderBy es "+campos.length+" elementos");
            campoOrdenacion = campos[0];
            sentidoOrdenacion = (campos.length > 1) ? campos[1] : null;
        }

        // Obtenemos lista de coches ordenada
        List<CocheModel> listaCoches = this.cocheService.findAllOrdered(campoOrdenacion, sentidoOrdenacion);

        // Personalizamos mensaje de salida en función de si el consumidor ha indicado algún campo por el que ordenar
        StringBuffer mensajeConsumidor = new StringBuffer("Consulta realizada con éxito");
        if (campoOrdenacion != null)
            mensajeConsumidor.append(" y ordenada por el campo <"+campoOrdenacion+">");


        //Creamos el objeto a devolver al consumidor
        httpMessage = new HttpMessage(new Date().toString(), HttpStatus.OK, mensajeConsumidor.toString(), "/sostycar/coches", listaCoches);

        return new ResponseEntity(httpMessage, httpMessage.getStatus());
    }




    @GetMapping("/coches/{id}")
    public ResponseEntity<Object> getCocheById(@PathVariable String id) {
        System.out.println("getCocheById in CocheController");
        System.out.println("El coche para buscar es " + id);

        Optional<CocheModel> result = cocheService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Coche no encontrado!!",
                result.isPresent() ? HttpStatus.OK :  HttpStatus.NOT_FOUND
        );
    }


    @PostMapping("/coches")
    public ResponseEntity<CocheModel> addCoche(@RequestBody CocheModel coche) {
        System.out.println("addCoche in CocheController");
        System.out.println("El coche para crear es " + coche.toString());

        return new ResponseEntity<>(this.cocheService.addCoche(coche),
                HttpStatus.CREATED);
    }

    @PutMapping("/coches/{id}")
    public ResponseEntity<CocheModel> updateCoche(@RequestBody CocheModel coche,
                                                  @PathVariable String id) {
        System.out.println("updateCoche in CocheController");
        System.out.println("El coche para modificar es " + coche.toString());

        Optional<CocheModel> cocheToUpdate = cocheService.findById(id);

        if (cocheToUpdate.isPresent()) {
            System.out.println("Coche a modificar encontrado...Modificando...");
            this.cocheService.update(coche);
        }

        return new ResponseEntity<>(coche,
                cocheToUpdate.isPresent() ? HttpStatus.OK :  HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/coches/{id}")
    public ResponseEntity<String> deleteCoche(@PathVariable String id) {
        System.out.println("deleteCoche in CocheController");
        System.out.println("El id coche para borrar es " + id);

        boolean deletedCoche = this.cocheService.deleteCoche(id);

        return new ResponseEntity<>(
                deletedCoche ? "Coche borrado!!!" : "Coche a borrar no encontrado!!",
                deletedCoche ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}




