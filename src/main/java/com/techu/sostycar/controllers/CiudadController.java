package com.techu.sostycar.controllers;


import com.techu.sostycar.models.CiudadModel;
import com.techu.sostycar.models.DistanciaModel;
import com.techu.sostycar.services.CiudadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Clase controladora para el recurso "ciudades"
 */
@RestController
@RequestMapping("/sostycar/")
public class CiudadController {

    /* Servicio de negocio para operar con el recurso "Ciudad" */
    @Autowired
    CiudadService ciudadService;

    /**
     * Crea una nueva ciudad
     * @param ciudadModel Objeto que encapsula los datos de la ciudad a crear
     * @return Código HTTP 200 - JSON con los datos de la ciudad que ha sido creada
     *         Código HTTP 409 - Cuando no se superan las validaciones de negocio para crear la ciudad
     *         Código HTTP 400 - Cuando los datos recibidos no son correctos para realizar la operación
     */
    @PostMapping("ciudades")
    public ResponseEntity<HttpMessage> addCiudad(@RequestBody CiudadModel ciudadModel){
        System.out.println("addCiudad...");

        ResponseEntity<HttpMessage> responseEntity;
        HttpMessage httpMessage;
        ciudadService.addCiudad(ciudadModel);

        httpMessage = new HttpMessage(new Date().toString(), HttpStatus.CREATED, "Ciudad dada de alta en el sistema", "/sostycar/ciudades", ciudadModel);
        //httpMessage = new HttpMessage(String.valueOf(System.currentTimeMillis()), HttpStatus.CONFLICT, "La compra no ha podido realizarse", "/sostycar/ciudades", erroresEncontrados);
        //httpMessage = new HttpMessage(String.valueOf(System.currentTimeMillis()), HttpStatus.BAD_REQUEST,"Datos de entrada incorrectos", "/sostycar/ciudades", null);

        return new ResponseEntity(httpMessage, httpMessage.getStatus());
    }

    /**
     * Recupera los datos de una ciudad dado su identificador
     * @param id Identificador de la ciudad a recuperar
     * @return Código HTTP 200 - JSON con los datos de la ciudad cuyo identificador coincide con el facilitado
     *         Código HTTP 404 - Cuando el recurso ciudad no ha sido encontrado
     */
    @GetMapping("ciudades/{id}")
    public ResponseEntity<HttpMessage> getCiudadById(@PathVariable String id){
        System.out.println("getUserById");
        System.out.println("Id es:"+id);

        ResponseEntity<HttpMessage> responseEntity;
        HttpMessage httpMessage;

        Optional<CiudadModel> result = this.ciudadService.getCiudadById(id);

        if (result.isPresent())
            httpMessage = new HttpMessage(new Date().toString(), HttpStatus.OK, "Ciudad recuperada con éxito", "/sostycar/ciudades", result.get());
        else
            httpMessage = new HttpMessage(new Date().toString(), HttpStatus.NOT_FOUND, "Ciudad no encontrada", "/sostycar/ciudades", "");


        return new ResponseEntity(httpMessage, httpMessage.getStatus());
    }

    /**
     * Obtiene todas las ciudades y las distancias al resto
     *
     * @return Código HTTP 200 - Una lista de objetos JSON que contienen los datos de cada ciudad.
     */
    @GetMapping("ciudades")
    public ResponseEntity<HttpMessage> getCiudades(){

        System.out.println("getCiudades en CiudadController...");
        ResponseEntity<HttpMessage> responseEntity;
        HttpMessage httpMessage;

        List<CiudadModel> listaCiudades = this.ciudadService.findAll();

        httpMessage = new HttpMessage(new Date().toString(), HttpStatus.OK, "Consulta realizada con éxito", "/sostycar/ciudades", listaCiudades);

        return new ResponseEntity(httpMessage, httpMessage.getStatus());
    }

}
