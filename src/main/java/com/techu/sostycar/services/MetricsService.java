package com.techu.sostycar.services;

import com.techu.sostycar.models.AhorroCocheBean;
import com.techu.sostycar.models.CiudadModel;
import com.techu.sostycar.models.CocheModel;
import com.techu.sostycar.models.TrayectoModel;
import com.techu.sostycar.repositories.CocheRepository;
import com.techu.sostycar.repositories.TrayectoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Clase de negocio para la obtención de métricas
 */
@Service
public class MetricsService {

    /* Servicio para operar con ciudades */
    @Autowired
    CiudadService ciudadService;

    /* Servicio para operar con coches */
    @Autowired
    CocheService cocheService;

    /* Servicio para operar con trayectos */
    @Autowired
    TrayectoService trayectoService;

    /* Interfaz para operar contra el repositorio de coches */
    @Autowired
    CocheRepository cocheRepository;

    /* Interfaz para operar contra el repositorio de coches */
    @Autowired
    TrayectoRepository trayectoRepository;


    /**
     * Obtiene la lista de coches de mi flota ordenados de menor a mayor consumo y añadiendo el dato del ahorro realizado con respecto
     * al uso de la versión gasolina del mismo vehículo.
     * @return Una lista que encapsula la información descrita
     */
    public List<AhorroCocheBean> getListaCochesMenosConsumoConAhorro(){
        System.out.println("getListaCochesMenosConsumoConAhorro en MetricsService");

        List<CocheModel> listaCoches = this.cocheService.findAllOrdered("consumoAcumuladoE","ASC");
        List<AhorroCocheBean> listaCochesConAhorro = new ArrayList();

        for (CocheModel cocheModel : listaCoches){
            // Calculamos ahorro y lo añadimos

            float ahorroEuros = cocheModel.getCosteSimuladoG()-cocheModel.getCosteAcumuladoE();
            AhorroCocheBean ahorroCoche = new AhorroCocheBean(cocheModel, Float.valueOf(ahorroEuros));
            listaCochesConAhorro.add(ahorroCoche);
        }

        return listaCochesConAhorro;
    }

    /**
     * Obtiene la lista de ciudades ordenadas por el número de visitas que han tenido y cuales han sido las ciudades origen
     * @return Una lista que encapsula la información descrita
     */
    public List<VisitasPorCiudadMetric> getCiudadesMasVisitadas(){
        System.out.println("getCiudadesMasVisitadas en MetricsService");

        List<TrayectoModel> listaTrayectos = trayectoService.findAll();
        List<String> idsCiudadesDestino = new ArrayList();
        Map<String, VisitasPorCiudadMetric> contadorVisitasPorCiudad = new HashMap();
        //Map<String, String> mapDuplicados

        // Extraigo las ciudades destino y contabilizo las visitas
        for (TrayectoModel trayecto : listaTrayectos){

            // Si ya tiene alguna visita contabilizada
            if (contadorVisitasPorCiudad.get(trayecto.getIdCiudadDestino()) != null) {

                // Incremento el número de visitas y añado la ciudadOrigen
                Optional<CiudadModel> optionalCiudadOrigen = ciudadService.getCiudadById(trayecto.getIdCiudadOrigen());
                if (optionalCiudadOrigen.isPresent()){ // Si la ciudad origen está en el repo la añado a la lista y contabilizo una visita más

                    optionalCiudadOrigen.get().setDistancias(new ArrayList()); // Limpio las distancias porque no las queremos en la métrica

                    contadorVisitasPorCiudad.get(trayecto.getIdCiudadDestino()).getCiudadesOrigen().add(optionalCiudadOrigen.get());
                    int contadorVisitas = contadorVisitasPorCiudad.get(trayecto.getIdCiudadDestino()).getNumVisitas().intValue()+1;
                    contadorVisitasPorCiudad.get(trayecto.getIdCiudadDestino()).setNumVisitas(Integer.valueOf(contadorVisitas));
                }
                else
                    System.out.println("La ciudad origen con ID "+trayecto.getIdCiudadOrigen() + "no se encuentra en el repo de ciudades");
            }

            // Es la primera vez que encontramos la ciudad destino en la lista de trayectos
            else{
                // Busco la ciudad destino y origen
                Optional<CiudadModel> optionalCiudadDestino = ciudadService.getCiudadById(trayecto.getIdCiudadDestino());
                Optional<CiudadModel> optionalCiudadOrigen = ciudadService.getCiudadById(trayecto.getIdCiudadOrigen());

                // Si la ciudad destino existe en el repositorio de ciudades creo el primer objeto para la metrica de esa ciudad destino
                if (optionalCiudadDestino.isPresent()){
                    List<CiudadModel> listaCiudadesOrigen = new ArrayList();

                    // Si la ciudad origen está en el repo
                    if(optionalCiudadOrigen.get() != null){
                        optionalCiudadOrigen.get().setDistancias(new ArrayList()); // Limpio las distancias porque no las queremos en la métrica
                        listaCiudadesOrigen.add(optionalCiudadOrigen.get()); // Añado la ciudad origen
                    }
                    else
                        listaCiudadesOrigen.add(new CiudadModel("N/C","N/C","N/C", new ArrayList())); // Añado un origen desconocido


                    // Añado la primera visita al Map
                    VisitasPorCiudadMetric visitasPorCiudadMetric = new VisitasPorCiudadMetric(optionalCiudadDestino.get().getId(),optionalCiudadDestino.get().getNombre(),Integer.valueOf(1), listaCiudadesOrigen);
                    contadorVisitasPorCiudad.put(optionalCiudadDestino.get().getId(),visitasPorCiudadMetric);
                }
                else // Si la ciudad destino no está presente en el repo, ni guardo ni contabilizo nada
                    System.out.println("La ciudad destino con ID "+trayecto.getIdCiudadDestino() + "no se encuentra en el repo de ciudades");

            }
        }

        // Ya tengo un HashMap con los ids de las ciudades destino y el la métrica de cada una de ellas, así que monto la lista a devolver
        List<VisitasPorCiudadMetric> listaDeMetricasResultado = new ArrayList();
        Set<String> setCiudadesDestino = contadorVisitasPorCiudad.keySet();

        for (String idCiudadDestino : setCiudadesDestino){
            listaDeMetricasResultado.add(contadorVisitasPorCiudad.get(idCiudadDestino));
        }

        // Ordeno la lista gracias a la implementación de la interfaz comparable
        Collections.sort(listaDeMetricasResultado);

        return listaDeMetricasResultado;
    }

}
