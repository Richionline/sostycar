package com.techu.sostycar.services;

import com.techu.sostycar.models.CocheModel;
import com.techu.sostycar.models.TrayectoModel;
import com.techu.sostycar.repositories.TrayectoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TrayectoService {

    @Autowired
    TrayectoRepository trayectoRepository;

    @Autowired
    ModeloService modeloService;

    @Autowired
    CocheService cocheService;

    // Obtener todos los trayectos
    public List<TrayectoModel> findAll() {
        System.out.println("findAll in TrayectoService");

        return this.trayectoRepository.findAll(Sort.by(Sort.Direction.DESC,"kms"));
    }


    // Obtener un determinado trayecto
    public Optional<TrayectoModel> findById(String id) {
        System.out.println("findById in TrayectoService");

        return this.trayectoRepository.findById(id);
    }


    // Crear una nueva trayectoria
    public TrayectoModel addTrayecto(TrayectoModel trayecto) {
        System.out.println("addTrayectoria in TrayectoService");

        // Actualizamos costes electricos
        trayecto.setCosteRealE(trayecto.getConsumoKW() * trayecto.getPrecioKW());

        // Actualizamos costes gasolina
        trayecto.setCosteComparativoG(trayecto.getConsumoSimuladoG() * trayecto.getPrecioGasolina());

        // Actualizamos acumulados del coche
        if (cocheService.findById(trayecto.getIdCoche()).isPresent()) {
            System.out.println("Actualizamos coche con datos trayecto...");
            CocheModel cocheUpdate = cocheService.findById(trayecto.getIdCoche()).get();

            cocheUpdate.setConsumoAcumuladoE(cocheUpdate.getConsumoAcumuladoE() + trayecto.getConsumoKW());
            cocheUpdate.setConsumoSimuladoG(cocheUpdate.getConsumoSimuladoG() + trayecto.getConsumoSimuladoG());
            cocheUpdate.setCosteAcumuladoE(cocheUpdate.getCosteAcumuladoE() + trayecto.getCosteRealE());
            cocheUpdate.setCosteSimuladoG(cocheUpdate.getCosteSimuladoG() + trayecto.getCosteComparativoG());
            cocheUpdate.setKmRecorridos(cocheUpdate.getKmRecorridos() + trayecto.getKms());

            cocheService.update(cocheUpdate);
        }

        return this.trayectoRepository.save(trayecto);
    }


    // Obtener trayectos de un determinado coche
    public List<TrayectoModel> findByIdCoche(String id) {
        System.out.println("findByIdCoche in TrayectoService");


        //return this.trayectoRepository.findByIdCoche(id);
        return this.trayectoRepository.findByIdCoche(id);
    }

    private CocheDatosService getTotals(List<TrayectoModel> listTrayectos) {

        float kms = 0;
        float consumoKW = 0;
        float precioKW = 0;
        float costeRealE = 0;
        float consumoSimuladoG = 0;
        float precioGasolina = 0;
        float costeComparativoG = 0;

        for(TrayectoModel item: listTrayectos) {
            kms += item.getKms();
            consumoKW += item.getConsumoKW();
            precioKW += item.getPrecioKW();
            costeRealE += item.getCosteRealE();
            consumoSimuladoG += item.getConsumoSimuladoG();
            precioGasolina += item.getPrecioGasolina();
            costeComparativoG += item.getCosteComparativoG();
        }

        precioKW = precioKW / listTrayectos.size();
        precioGasolina = precioGasolina / listTrayectos.size();

        CocheDatosService cocheDatosService = new CocheDatosService();
        cocheDatosService.setKms(kms);
        cocheDatosService.setConsumoKW(consumoKW);
        cocheDatosService.setPrecioKW(precioKW);
        cocheDatosService.setCosteRealE(costeRealE);
        cocheDatosService.setConsumoSimuladoG(consumoSimuladoG);
        cocheDatosService.setPrecioGasolina(precioGasolina);
        cocheDatosService.setCosteComparativoG(costeComparativoG);

        return cocheDatosService;

    }

    // Obtener trayectos de un determinado coche
    public CocheDatosService findByIdCocheGroup(String id) {
        System.out.println("findByIdCocheGroup in TrayectoService");

        List<TrayectoModel> listTrayectos = this.trayectoRepository.findByIdCoche(id);

        CocheDatosService cocheDatosService = getTotals(listTrayectos);
        cocheDatosService.setIdCoche(id);
        cocheDatosService.setDescripcionModelo(modeloService.findByid(id).get().getDescripcion());

        return cocheDatosService;

    }

    // Obtener trayectos agrupados
    public CocheDatosService findByGroup() {
        System.out.println("findByGroup in TrayectoService");

        List<TrayectoModel> listTrayectos = this.trayectoRepository.findAll();

        CocheDatosService cocheDatosService = getTotals(listTrayectos);
        cocheDatosService.setIdCoche("TOTAL");
        cocheDatosService.setDescripcionModelo("TOTAL");

        return cocheDatosService;

    }
}
